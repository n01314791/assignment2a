﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment2_A_AnjaliPanchal
{
    public partial class DigitalDesign : System.Web.UI.Page
    {
        DataView CreateDataSourceNote()
        {
            DataTable codedata = new DataTable();

            //First Column is the line number of code (idx -> index)
            DataColumn idx_col = new DataColumn();

            //Second column is the actual code itself
            DataColumn code_col = new DataColumn();

            DataRow coderow;

            idx_col.ColumnName = "Line";
            idx_col.DataType = System.Type.GetType("System.Int32");

            codedata.Columns.Add(idx_col);

            code_col.ColumnName = "Code";
            code_col.DataType = System.Type.GetType("System.String");
            codedata.Columns.Add(code_col);
            //List of code

            List<string> web_program_code = new List<string>(new string[] {
                "In the following example all",
                "elements will be center-aligned, ",
                "with a red text color:",
                "p {",
                "~color: red;",
                "~text-align: center;",
                "} ",
                "Select and style all elements with class=\"intro\":",
                ".intro { ",
                "~background-color: yellow;",
                "} "
            });

            int i = 0;
            foreach (string code_line in web_program_code)
            {
                coderow = codedata.NewRow();
                coderow[idx_col.ColumnName] = i;
                string formatted_code = System.Net.WebUtility.HtmlEncode(code_line);
                formatted_code = formatted_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                coderow[code_col.ColumnName] = formatted_code;

                i++;
                codedata.Rows.Add(coderow);
            }

            DataView codeview = new DataView(codedata);
            return codeview;

        }

        DataView CreateDataSourceExample()
        {
            DataTable codedata = new DataTable();

            //First Column is the line number of code (idx -> index)
            DataColumn idx_col = new DataColumn();

            //Second column is the actual code itself
            DataColumn code_col = new DataColumn();

            DataRow coderow;

            idx_col.ColumnName = "Line";
            idx_col.DataType = System.Type.GetType("System.Int32");

            codedata.Columns.Add(idx_col);

            code_col.ColumnName = "Code";
            code_col.DataType = System.Type.GetType("System.String");
            codedata.Columns.Add(code_col);
            //List of code

            List<string> web_program_code = new List<string>(new string[] {
                "The HTML class attribute is used to define equal  ",
                "styles for elements with the same class name.",
                "So, all HTML elements with the same class ",
                "attribute will have the same format and style.",
                "Here we have three <div> elements that ",
                "point to the same class name:",
                "~.cities {    ",
                "~~background-color: black; ",
                "~~color: white; ",
                "~~margin: 20px; ",
                "~~padding: 20px;",
                "~} ",
                "~<div class=\"cities\">  ",
                "~<h2>London</h2>",
                "~<p>London is the capital of England.</p>",

            });

            int i = 0;
            foreach (string code_line in web_program_code)
            {
                coderow = codedata.NewRow();
                coderow[idx_col.ColumnName] = i;
                string formatted_code = System.Net.WebUtility.HtmlEncode(code_line);
                formatted_code = formatted_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                coderow[code_col.ColumnName] = formatted_code;

                i++;
                codedata.Rows.Add(coderow);
            }

            DataView codeview = new DataView(codedata);
            return codeview;


        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Grid for Notes
            DataView ds = CreateDataSourceNote();
            Note_DigitalDesign.DataSource = ds;
            Note_DigitalDesign.DataBind();

            //Grid for Example
            DataView ds_eg = CreateDataSourceExample();
            Example_DigitalDesign.DataSource = ds_eg;
            Example_DigitalDesign.DataBind();

        }
    }
}