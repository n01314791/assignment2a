﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Assignment2_A_AnjaliPanchal
{
    public partial class WebProgramming : System.Web.UI.Page
    {
        DataView CreateDataSourceNote()
        {
            DataTable codedata = new DataTable();

            //First Column is the line number of code (idx -> index)
            DataColumn idx_col = new DataColumn();

            //Second column is the actual code itself
            DataColumn code_col = new DataColumn();

            DataRow coderow;

            idx_col.ColumnName = "Line";
            idx_col.DataType = System.Type.GetType("System.Int32");

            codedata.Columns.Add(idx_col);

            code_col.ColumnName = "Code";
            code_col.DataType = System.Type.GetType("System.String");
            codedata.Columns.Add(code_col);
            //List of code

            List<string> web_program_code = new List<string>(new string[] {
                "function myFirstFunction (p1 , p2 ) {",
                "~return p1 * p2 ; ",
                "} ",
                "",
                "var person = { ",
                "~firstName : \"Anjali\",",
                "~lastName : \"Panchal\",",
                "~id : 01314791,",
                "~fullName : function ()  { ",
                "~~return firstName + \" \" + lastName ;",
                "~} ",
                "}; "
            });

            int i = 0;
            foreach (string code_line in web_program_code)
            {
                coderow = codedata.NewRow();
                coderow[idx_col.ColumnName] = i;
                string formatted_code = System.Net.WebUtility.HtmlEncode(code_line);
                formatted_code = formatted_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                coderow[code_col.ColumnName] = formatted_code;

                i++;
                codedata.Rows.Add(coderow);
            }

            DataView codeview = new DataView(codedata);
            return codeview;
        }

        DataView CreateDataSourceExample()
        {
            DataTable codedata = new DataTable();

            //First Column is the line number of code (idx -> index)
            DataColumn idx_col = new DataColumn();

            //Second column is the actual code itself
            DataColumn code_col = new DataColumn();

            DataRow coderow;

            idx_col.ColumnName = "Line";
            idx_col.DataType = System.Type.GetType("System.Int32");

            codedata.Columns.Add(idx_col);

            code_col.ColumnName = "Code";
            code_col.DataType = System.Type.GetType("System.String");
            codedata.Columns.Add(code_col);
            //List of code

            List<string> web_program_code = new List<string>(new string[] {
                "I Have Taken reference Example from website https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions ",
                "The Example is as below :",
                "/* Declare the function 'myFunc' */ ",
                "function myFunc() {",
                "~theObject.brand = \"Toyota\";",
                "} ",
                "var mycar = { ",
                "~brand: \"Honda\", ",
                "~model: \"Accord\", ",
                "~year: 1998, ",
                "~} ;",
                "~console.log(mycar.brand);",
                "}; "
            });

            int i = 0;
            foreach (string code_line in web_program_code)
            {
                coderow = codedata.NewRow();
                coderow[idx_col.ColumnName] = i;
                string formatted_code = System.Net.WebUtility.HtmlEncode(code_line);
                formatted_code = formatted_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                coderow[code_col.ColumnName] = formatted_code;

                i++;
                codedata.Rows.Add(coderow);
            }

            DataView codeview = new DataView(codedata);
            return codeview;

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //Grid for Notes
            DataView ds = CreateDataSourceNote();
            Note_WebProgram.DataSource = ds;
            Note_WebProgram.DataBind();

            //Grid for Example
            DataView ds_eg = CreateDataSourceExample();
            Example_WebProgram.DataSource = ds_eg;
            Example_WebProgram.DataBind();
        }
    }
}