﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment2_A_AnjaliPanchal
{
    public partial class DatabaseDesign : System.Web.UI.Page
    {
        DataView CreateDataSourceNote()
        {
            DataTable codedata = new DataTable();

            //First Column is the line number of code (idx -> index)
            DataColumn idx_col = new DataColumn();

            //Second column is the actual code itself
            DataColumn code_col = new DataColumn();

            DataRow coderow;

            idx_col.ColumnName = "Line";
            idx_col.DataType = System.Type.GetType("System.Int32");

            codedata.Columns.Add(idx_col);

            code_col.ColumnName = "Code";
            code_col.DataType = System.Type.GetType("System.String");
            codedata.Columns.Add(code_col);
            //List of code

            List<string> web_program_code = new List<string>(new string[] {
                "Chances are, you've already written a ",
                "statement that uses an Oracle INNER JOIN",
                "It is the most common type of join. ",
                "Oracle INNER JOINS return all rows from ",
                "multiple tables where the join condition is met.",
                "Syntax ",
                "Example 1",
                "~SELECT columns",
                "~FROM table1 ",
                "~INNER JOIN table2 ",
                "~ON table1.column = table2.column; ",
                "Example 2",
                "~SELECT columns",
                "~FROM table1 ",
                "~Left JOIN table2 ",
                "~ON table1.column = table2.column; "
            });

            int i = 0;
            foreach (string code_line in web_program_code)
            {
                coderow = codedata.NewRow();
                coderow[idx_col.ColumnName] = i;
                string formatted_code = System.Net.WebUtility.HtmlEncode(code_line);
                formatted_code = formatted_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                coderow[code_col.ColumnName] = formatted_code;

                i++;
                codedata.Rows.Add(coderow);
            }

            DataView codeview = new DataView(codedata);
            return codeview;
        }

        DataView CreateDataSourceExample()
        {
            DataTable codedata = new DataTable();

            //First Column is the line number of code (idx -> index)
            DataColumn idx_col = new DataColumn();

            //Second column is the actual code itself
            DataColumn code_col = new DataColumn();

            DataRow coderow;

            idx_col.ColumnName = "Line";
            idx_col.DataType = System.Type.GetType("System.Int32");

            codedata.Columns.Add(idx_col);

            code_col.ColumnName = "Code";
            code_col.DataType = System.Type.GetType("System.String");
            codedata.Columns.Add(code_col);
            //List of code

            List<string> web_program_code = new List<string>(new string[] {
                "Example 1",
                "~select clients.clientid, clientfname, clientlname,  ",
                "~SUM(invoices.invoice_amount)  ",
                "~from clients ",
                "~inner join invoices on ",
                "~invoices.clientid = clients.clientid",
                "~group by clients.clientid, ",
                "~clients.clientfname,clientlname",
                "~order by clients.clientid;",
                "Example 2",
                "~select clients.clientid, clientfname, clientlname, ",
                "~SUM(invoices.invoice_amount)  ",
                "~from clients ",
                "~left join invoices on ",
                "~invoices.clientid = clients.clientid",
                "~group by clients.clientid,",
                "~clients.clientfname,clientlname",
                "~order by clients.clientid;"
            });

            int i = 0;
            foreach (string code_line in web_program_code)
            {
                coderow = codedata.NewRow();
                coderow[idx_col.ColumnName] = i;
                string formatted_code = System.Net.WebUtility.HtmlEncode(code_line);
                formatted_code = formatted_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                coderow[code_col.ColumnName] = formatted_code;

                i++;
                codedata.Rows.Add(coderow);
            }

            DataView codeview = new DataView(codedata);
            return codeview;

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Grid for Notes
            DataView ds = CreateDataSourceNote();
            Note_Database.DataSource = ds;
            Note_Database.DataBind();

            //Grid for Example
            DataView ds_eg = CreateDataSourceExample();
            Example_Database.DataSource = ds_eg;
            Example_Database.DataBind();
        }
    }
}