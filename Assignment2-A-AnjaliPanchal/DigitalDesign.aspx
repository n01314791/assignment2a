﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DigitalDesign.aspx.cs" Inherits="Assignment2_A_AnjaliPanchal.DigitalDesign" %>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="maincss" runat="server">
</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="Menu" runat="server">
    <h2>Intro to CSS</h2>
    <div>
        <p>CSS stands for Cascading Style Sheets</p>
        <p>CSS describes how HTML elements are to be displayed on screen, paper, or in other media</p>
        <p>CSS saves a lot of work. It can control the layout of multiple web pages all at once</p>
        <p>External stylesheets are stored in CSS files</p>
        <p>CSS is used to define styles for your web pages, including the design, layout and variations in display for different devices and screen sizes. </p>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Notes</h2>
    <asp:DataGrid ID="Note_DigitalDesign" runat="server" CssClass="code"
        GridLines="None" Width="350px" CellPadding="2">
        <HeaderStyle Font-Size="Large" BackColor="#999966" ForeColor="White" />
        <ItemStyle BackColor="#1a1d23" ForeColor="#ffffff" />
    </asp:DataGrid>

    <%--<div>
        <p>
            In the following example all
        elements will be center-aligned, with a red text color:
        </p>
        <p>p {</p>
        <p>color: red;</p>
        <p>text-align: center;</p>
        <p>}</p>
        <p>Select and style all elements with class="intro":</p>
        <p>.intro { </p>
        <p>background-color: yellow;</p>
        <p>}</p>
    </div>--%>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Sidebar" runat="server">
    <h2>Example</h2>
    <asp:DataGrid ID="Example_DigitalDesign" runat="server" CssClass="code"
        GridLines="None" Width="350px" CellPadding="2">
        <HeaderStyle Font-Size="Large" BackColor="#999966" ForeColor="White" />
        <ItemStyle BackColor="#1a1d23" ForeColor="#ffffff" />
    </asp:DataGrid>

    <%--<div>
        <p>The HTML class attribute is used to define equal styles for elements with the same class name.</p>
        <p>So, all HTML elements with the same class attribute will have the same format and style.</p>
        <p>Here we have three &lt;div&gt; elements that point to the same class name:</p>
        <p>.cities {    </p>
        <p>background-color: black; </p>
        <p>color: white; </p>
        <p>margin: 20px; </p>
        <p>padding: 20px;</p>
        <p>}                            </p>
        <p>&lt;div class="cities"&gt;   </p>
        <p>&lt;h2&gt;London&lt;/h2&gt;</p>
        <p>&lt;p&gt;London is the capital of England.&lt;/p&gt;</p> 
    </div>--%>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Footer" runat="server">
    <h2>Useful Links</h2>
    <div>
        <p><a href="https://www.w3schools.com/css/css_intro.asp" title="Click Here">https://www.w3schools.com/css/css_intro.asp</a></p>
        <p><a href="https://developer.mozilla.org/en-US/docs/Learn/CSS/Introduction_to_CSS" title="Click Here">https://developer.mozilla.org/en-US/docs/Learn/CSS/Introduction_to_CSS</a></p>
    </div>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="Feedback" runat="server">
    <uctrl:feedback runat="server" Message="Thank You for your feedback" ID="CodeBox" />
</asp:Content>
