﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WebProgramming.aspx.cs" Inherits="Assignment2_A_AnjaliPanchal.WebProgramming" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Menu" runat="server">
    <div id="body">
        <h2>Functions And Objects</h2>
        <div class="col-md-10 ">
            <p>A Javascript function is a block of code designed to perform a particular task</p>
            <p>A JavaScript function is executed when "something" invokes it (calls it).</p>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Notes</h2>
    <asp:DataGrid ID="Note_WebProgram" runat="server" CssClass="code"
        GridLines="None" Width="350px" CellPadding="2">
        <HeaderStyle Font-Size="Large" BackColor="#999966" ForeColor="White" />
        <ItemStyle BackColor="#1a1d23" ForeColor="#ffffff" />
    </asp:DataGrid>
    <%--    <div>
        <span>function myFirstFunction (p1 , p2 ) { </span>
        <br />
        <span>return p1 * p2 ; </span>
        <br />
        <span>} </span>
    </div>
    <div>
        <br />
        <span>var person = { </span>
        <br />
        <span>firstName : "Anjali",</span>
        <br />
        <span>lastName : "Panchal",</span>
        <br />
        <span>id : 01314791,</span>
        <br />
        <span>fullName : function ()  { </span>
        <br />
        <span>return firstName + " " + lastName ;</span>
        <br />
        <span>} </span>
        <br />
        <span>}; </span>

    </div>--%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Sidebar" runat="server">
    <h2>Example</h2>
    <asp:DataGrid ID="Example_WebProgram" runat="server" CssClass="code"
        GridLines="None" Width="350px" CellPadding="2">
        <HeaderStyle Font-Size="Large" BackColor="#999966" ForeColor="White" />
        <ItemStyle BackColor="#1a1d23" ForeColor="#ffffff" />
    </asp:DataGrid>
    <%--<div>
        <p>I Have Taken reference Example from website https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions </p>
        <p>The Example is as below :</p>
        <br />
        <span class="comment">/* Declare the function 'myFunc' */ </span>
        <br />
        <span class="keyword">function </span><span class="function">myFunc </span><span class="punctuation">(</span> theObject <span class="punctuation">) </span><span class="punctuation">{</span>
        <br />
        <span>theObject</span><span class="punctuation">.</span><span>brand</span><span class="operator"> = </span><span class="string"> "Toyota";</span>
        <br />
        <span>} </span>
        <br />
        <span class="keyword">var</span> mycar <span class="operator"> = </span><span class="punctuation"> { </span>
        <br />
        <span>brand: "Honda", </span>
        <br />
        <span>model: "Accord", </span>

        <br />
        <span>year: 1998, </span>
        <br />
        <span>} ;</span>
        <br />
        <span>console.log(mycar.brand);</span>
    </div>--%>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Footer" runat="server">
    <h2>Useful Links</h2>
    <a href="https://www.w3schools.com/js/js_functions.asp" title="Click here">https://www.w3schools.com/js/js_functions.asp</a>
    <br />
    <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions" title="Click here">https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions</a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Feedback" runat="server">
    <uctrl:feedback runat="server" Message="Thank You for your feedback" ID="CodeBox" />
</asp:Content>
