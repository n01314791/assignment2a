﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CodeBox.ascx.cs" Inherits="Assignment2_A_AnjaliPanchal.User_control.CodeBox" %>

<div id="footer">
    <h2>Feedback</h2>
    <div>
        <div class="row">
            <div class="col-md-2 col-xs-12">
                <label class="formlabel">Name:</label>
            </div>
            <div class="col-md-10 col-xs-12">
                <asp:TextBox runat="server" ID="name"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="name"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 col-xs-12">
                <label class="formlabel">Message:</label>
            </div>
            <div class="col-md-10 col-xs-12">
                <asp:TextBox runat="server" ID="message"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="message"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="row">
            <asp:Button runat="server" OnClick="Submit_Form" Text="Submit" />
        </div>
        <asp:Label runat="server" ID="thnk_you"></asp:Label>

    </div>
</div>
