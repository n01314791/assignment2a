﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment2_A_AnjaliPanchal.User_control
{
    public partial class CodeBox : System.Web.UI.UserControl
    {
        public string Message
        {
            get { return (string)ViewState["Message"]; }
            set { ViewState["Message"] = value; }

        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Submit_Form(object sender, EventArgs e)
        {
            thnk_you.Text = Message;
        }
    }
}