﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DatabaseDesign.aspx.cs" Inherits="Assignment2_A_AnjaliPanchal.DatabaseDesign" %>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="maincss" runat="server">
    
</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="Menu" runat="server">
    <h2>Advanced Joins</h2>
    <div>
        <p>This Oracle tutorial explains how to use JOINS (inner and outer) in Oracle with syntax, visual illustrations, and examples.</p>
        <p>Oracle JOINS are used to retrieve data from multiple tables. An Oracle JOIN is performed whenever two or more tables are joined in a SQL statement.</p>
        <p>There are 4 different types of Oracle joins:</p>
        <ul>
            <li>Oracle INNER JOIN (or sometimes called simple join)</li>
            <li>Oracle LEFT OUTER JOIN (or sometimes called LEFT JOIN)</li>
            <li>Oracle RIGHT OUTER JOIN (or sometimes called RIGHT JOIN)</li>
            <li>Oracle FULL OUTER JOIN (or sometimes called FULL JOIN)</li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Notes</h2>
    <asp:DataGrid ID="Note_Database" runat="server" CssClass="code"
        GridLines="None" Width="350px" CellPadding="2">
        <HeaderStyle Font-Size="Large" BackColor="#999966" ForeColor="White" />
        <ItemStyle BackColor="#1a1d23" ForeColor="#ffffff" />
    </asp:DataGrid>
    <%--    <div>
        <p>Chances are, you've already written a statement that uses an Oracle INNER JOIN. It is the most common type of join. Oracle INNER JOINS return all rows from multiple tables where the join condition is met.</p>
        <p>Syntax </p>
        <p><span>1.</span> SELECT columns
FROM table1 
INNER JOIN table2
ON table1.column = table2.column;</p>
        <p><span>2.</span> SELECT columns
FROM table1 
LEFT JOIN table2
ON table1.column = table2.column;</p>
        </div>--%>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Sidebar" runat="server">
    <h2>Example</h2>
    <asp:DataGrid ID="Example_Database" runat="server" CssClass="code"
        GridLines="None" Width="350px" CellPadding="2">
        <HeaderStyle Font-Size="Large" BackColor="#999966" ForeColor="White" />
        <ItemStyle BackColor="#1a1d23" ForeColor="#ffffff" />
    </asp:DataGrid>

    <%--    <p>
        select clients.clientid, clientfname, clientlname,  SUM(invoices.invoice_amount)  from clients 
        inner join invoices on invoices.clientid = clients.clientid
        group by clients.clientid,clients.clientfname,clientlname order by clients.clientid;
        </p>--%>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Footer" runat="server">
    <h2>Useful Links</h2>
    <a href="https://www.techonthenet.com/oracle/joins.php" title="Click here">https://www.techonthenet.com/oracle/joins.php</a>
    <br />
    <a href="https://www.w3schools.com/sql/default.asp" title="Click here">https://www.w3schools.com/sql/default.asp</a>
    <br />
    <a href="http://www.dba-oracle.com/t_advanced_sql_full_outer_join.htm" title="Click Here">http://www.dba-oracle.com/t_advanced_sql_full_outer_join.htm</a>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="Feedback" runat="server">
    <uctrl:feedback runat="server" Message="Thank You for your feedback" ID="CodeBox" />
</asp:Content>
